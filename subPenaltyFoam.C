/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    vesicle

Description
    Transient solver for incompressible, laminar flow of Newtonian fluids.

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "IFstream.H"
#include "OFstream.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    #include "setRootCase.H"
    #include "createTime.H"
    #include "createMesh.H"
    #include "createFields.H"
	
	ofstream output;
	
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
	    
    // Create txt file for saving values

    Info<< "\nCreate txt file...\n" << endl;

    #include "input.H"


    Info<< "\nStarting time loop\n" << endl;
	
	int i=0;
	double alpha = 0;
	double beta = 0;

    while (runTime.loop())
    {

        Info<< "Time = " << runTime.timeName() << nl << endl;
		
		volScalarField sqrGradC1 = magSqr(fvc::grad(C1));
		
		// Calculate Reference Values for Area and Volume
		if (i == 0){
		
			// Initial Volume
			
			forAll(C1, celli){
				alpha += 0.5 * (C1[celli]*C1.mesh().V()[celli] + C1.mesh().V()[celli]);
			}
			
			
			// Initial Surface Area
			scalar intBeta = 0;
			
			
			forAll(C1, celli){
				intBeta +=((epsilon.value()/2)*sqrGradC1[celli] + (1/(4*(epsilon.value())))*pow((pow(C1[celli],2)-1),2)  )*C1.mesh().V()[celli];
			}
			
			
			beta = ( 3*pow(2,0.5)/4 )* intBeta;
			
		}

        // Volume
        scalar A = 0;

        forAll(C1,celli)
        {
            A += 0.5 * (C1[celli]*C1.mesh().V()[celli] + C1.mesh().V()[celli]);
        }


        // Area

        scalar intB = 0;
		

        forAll(C1,celli)
        {
            intB += ( (epsilon.value()/2)*sqrGradC1[celli] + (1/(4*(epsilon.value())))*pow( (pow(C1[celli],2)-1) ,2)  )*C1.mesh().V()[celli];
        }

        scalar B = ( 3*pow(2,0.5)/4 )*(intB);


        // Scheme

		volScalarField f = -epsilon*fvc::laplacian(C1) + (1/epsilon)*(pow(C1,2)-1)*C1;

        volScalarField g = -fvc::laplacian(f) + (1/pow(epsilon,2))*(3*pow(C1,2) - 1)*f;
		
		volScalarField h = (1/epsilon) * C1 * (pow(C2,2)-1);

        volScalarField varE = (k*g - sigma*h + M1*(A - alpha - d_alpha) + M2*(B - beta - d_beta)*f);


        // Gradient flow with penalty method

       	solve
        (
          fvm::ddt(C1) == -varE
        );	


        runTime.write();

        // Results file
        i++;

        if( i%100 == 0 ){
		
			// Free Energy
		
			dimensionedScalar intFE = 0;
			volScalarField outLapC = fvc::laplacian(C1);
			
			forAll(C1,celli){
            	intFE += pow( pow(C1[celli],3) - C1[celli] - pow(epsilon.value(),2)*outLapC[celli] ,2)*C1.mesh().V()[celli];
        	}

        	scalar FE = (( (3*k) / ( 2*pow(2,0.5)*pow(epsilon.value(),3) ) )*intFE).value();
			reduce(FE, sumOp<scalar>());
			
			// Adhesion Energy
			scalar Adh = 0;
			
			forAll(C1, celli){
				Adh = Adh + (sigma*((pow(C1[celli],2)-1)*(pow(C2[celli],2)-1)*C1.mesh().V()[celli])).value();
			}
			reduce(Adh, sumOp<scalar>());
			
			// Volume
			scalar outTotVol = 0;
        	scalar outIntC = 0;

        	forAll(C1,celli){
            	outTotVol += mag(mesh.V()[celli]);
            	outIntC += C1[celli]*C1.mesh().V()[celli];
        	}

        	scalar outVesicleVol = 0.5*( outTotVol + outIntC );
			reduce(outVesicleVol, sumOp<scalar>());
			
			// Surface Area
			scalar outIntB = 0;


        	forAll(C1,celli){
            	outIntB += ( (pow(epsilon.value(),2)/2)*sqrGradC1[celli] + (1/4)*pow( (pow(C1[celli],2)-1) ,2)  )*C1.mesh().V()[celli];
        	}

        	scalar outB = ( 3/(2*pow(2,0.5)*epsilon.value()) )*(outIntB);
			reduce(outB, sumOp<scalar>());
			
			
			output << FE;
        	output << ", ";
			output << Adh;
        	output << ", ";
        	output << outVesicleVol;
        	output << ", ";
        	output << outB;
        	output << "\n";
        	output.flush();
			
        }
        else
        {
            continue;
        }

    }

    output.close();

    Info<< "End\n" << endl;

    return 0;

}
// ************************************************************************* //
